/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createDailyentry = /* GraphQL */ `
  mutation CreateDailyentry(
    $input: CreateDailyentryInput!
    $condition: ModelDailyentryConditionInput
  ) {
    createDailyentry(input: $input, condition: $condition) {
      id
      uname
      fname
      chicks
      tchicks
      age
      mortality
      cmor
      fused
      fstock
      cfeed
      fintake
      avg
      fcr
      bbird
      complaint
      medicine
      createdAt
      updatedAt
      owner
    }
  }
`;
export const updateDailyentry = /* GraphQL */ `
  mutation UpdateDailyentry(
    $input: UpdateDailyentryInput!
    $condition: ModelDailyentryConditionInput
  ) {
    updateDailyentry(input: $input, condition: $condition) {
      id
      uname
      fname
      chicks
      tchicks
      age
      mortality
      cmor
      fused
      fstock
      cfeed
      fintake
      avg
      fcr
      bbird
      complaint
      medicine
      createdAt
      updatedAt
      owner
    }
  }
`;
export const deleteDailyentry = /* GraphQL */ `
  mutation DeleteDailyentry(
    $input: DeleteDailyentryInput!
    $condition: ModelDailyentryConditionInput
  ) {
    deleteDailyentry(input: $input, condition: $condition) {
      id
      uname
      fname
      chicks
      tchicks
      age
      mortality
      cmor
      fused
      fstock
      cfeed
      fintake
      avg
      fcr
      bbird
      complaint
      medicine
      createdAt
      updatedAt
      owner
    }
  }
`;

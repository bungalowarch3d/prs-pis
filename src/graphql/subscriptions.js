/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateDailyentry = /* GraphQL */ `
  subscription OnCreateDailyentry($owner: String!) {
    onCreateDailyentry(owner: $owner) {
      id
      uname
      fname
      chicks
      tchicks
      age
      mortality
      cmor
      fused
      fstock
      cfeed
      fintake
      avg
      fcr
      bbird
      complaint
      medicine
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdateDailyentry = /* GraphQL */ `
  subscription OnUpdateDailyentry($owner: String!) {
    onUpdateDailyentry(owner: $owner) {
      id
      uname
      fname
      chicks
      tchicks
      age
      mortality
      cmor
      fused
      fstock
      cfeed
      fintake
      avg
      fcr
      bbird
      complaint
      medicine
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeleteDailyentry = /* GraphQL */ `
  subscription OnDeleteDailyentry($owner: String!) {
    onDeleteDailyentry(owner: $owner) {
      id
      uname
      fname
      chicks
      tchicks
      age
      mortality
      cmor
      fused
      fstock
      cfeed
      fintake
      avg
      fcr
      bbird
      complaint
      medicine
      createdAt
      updatedAt
      owner
    }
  }
`;

/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getDailyentry = /* GraphQL */ `
  query GetDailyentry($id: ID!) {
    getDailyentry(id: $id) {
      id
      uname
      fname
      chicks
      tchicks
      age
      mortality
      cmor
      fused
      fstock
      cfeed
      fintake
      avg
      fcr
      bbird
      complaint
      medicine
      createdAt
      updatedAt
      owner
    }
  }
`;
export const listDailyentrys = /* GraphQL */ `
  query ListDailyentrys(
    $filter: ModelDailyentryFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listDailyentrys(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        uname
        fname
        chicks
        tchicks
        age
        mortality
        cmor
        fused
        fstock
        cfeed
        fintake
        avg
        fcr
        bbird
        complaint
        medicine
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;

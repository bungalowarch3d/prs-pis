import React, { Component } from 'react';
import {Button, Modal} from 'react-bootstrap'
import { Auth, API, graphqlOperation } from "aws-amplify";
import * as mutations from '../graphql/mutations';

class Editor extends Component {state = {
    modalshow: false,
    itemUser: '',itemFarm: '',itemChicks: '',itemTChicks: '',itemAge: '',itemMortality: '',itemCMor: '',itemFUsed: '',
    itemFStock: '',itemTFeed: '',itemFIn: '',itemAvg: '',itemFCR: '',itemBBirds: '',itemComplaint: '',itemMedicine: '',
  };
  
  handleClickOpen = () => {
    // console.log("Current Item: " + this.props.Item.id)
    this.setState({ modalshow: true  });
  };
  
  handleClose = () => {
    this.setState({ modalshow: false });
  };
  
  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };
  
  handleSubmit = (e) => {
    this.setState({ modalshow: false });
    const itemDetails = {
      id: this.props.Item.id,
      uname: this.state.itemUser || Auth.user.username,
      fname: this.state.itemFarm || this.props.Item.fname,
      chicks: this.state.itemChicks || this.props.Item.chicks,
      tchicks: this.state.itemTChicks || this.props.Item.tchicks,
      age: this.state.itemAge || this.props.Item.age,
      mortality: this.state.itemMortality || this.props.Item.mortality,
      cmor: this.state.itemCMor || this.props.Item.cmor,
      fused: this.state.itemFUsed || this.props.Item.fused,
      fstock: this.state.itemFStock || this.props.Item.fstock,
      cfeed: this.state.itemTFeed || this.props.Item.cfeed,
      fintake: this.state.itemFIn || this.props.Item.fintake,
      avg: this.state.itemAvg || this.props.Item.avg,
      fcr: this.state.itemFCR || this.props.Item.fcr,
      bbird: this.state.itemBBirds || this.props.Item.bbird,
      complaint: this.state.itemComplaint || this.props.Item.complaint,
      medicine: this.state.itemMedicine || this.props.Item.medicine,
    }
    API.graphql(graphqlOperation(mutations.updateDailyentry, {input: itemDetails}));
  }
  
  render() {
      return (
      <form style={{display: 'flex', flexWrap: 'wrap'}}>
      <Button variant="info" className="mr-3 mb-1" onClick={this.handleClickOpen}>Edit</Button>
        <Modal show={this.state.modalshow} onHide={this.handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>{this.props.Item.fname} On:{this.props.Item.createdAt}</Modal.Title>
            </Modal.Header>
          <Modal.Body>
              <ul>
            <li> <input
                style={{margin: 5}} id="itemUser" value={"Edit by: " + Auth.user.username}
                 label="User" readOnly="true"
                onChange={this.handleChange('itemUser')}
              /></li> 
              <li><label>Farm:</label><input
                style={{margin: 5, width:60}} id="itemFarm"
                placeholder={this.props.Item.fname} label="Farm"
                onChange={this.handleChange('itemFarm')}
              /></li>
              <li><label>Chicks:</label><input
                style={{margin: 5, width:60}} id="itemChicks"
                placeholder={this.props.Item.chicks} label="Chicks"
                onChange={this.handleChange('itemChicks')}
              /></li>
              <li><label>NOC:</label><input
                style={{margin: 5, width:60}} id="itemTChicks"
                placeholder={this.props.Item.tchicks} label="TChicks"
                onChange={this.handleChange('itemTChicks')}
              /></li>
              <li><label>Age:</label><input
                style={{margin: 5, width:60}} id="itemAge"
                placeholder={this.props.Item.age} label="Age"
                onChange={this.handleChange('itemAge')}
              /></li>
              <li><label>Mortality:</label><input
                style={{margin: 5, width:60}} id="itemMortality"
                placeholder={this.props.Item.mortality} label="Mortality"
                onChange={this.handleChange('itemMortality')}
              /></li>
              <li><label>C Mor:</label><input
                style={{margin: 5, width:60}} id="itemCMor"
                placeholder={this.props.Item.cmor} label="CMor"
                onChange={this.handleChange('itemCMor')}
              /></li>
              <li><label>Feed Used:</label><input
                style={{margin: 5, width:60}} id="itemFUsed"
                placeholder={this.props.Item.fused} label="FUsed"
                onChange={this.handleChange('itemFUsed')}
              /></li>
              <li><label>Feed Stock:</label><input
                style={{margin: 5, width:60}} id="itemFStock"
                placeholder={this.props.Item.fstock} label="FStock"
                onChange={this.handleChange('itemFStock')}
              /></li>
              <li><label>Total Feed:</label><input
                style={{margin: 5, width:60}} id="itemTFeed"
                placeholder={this.props.Item.cfeed} label="TFeed"
                onChange={this.handleChange('itemTFeed')}
              /></li>
              <li><label>Feed Intake:</label><input
                style={{margin: 5, width:60}} id="itemFIn"
                placeholder={this.props.Item.fintake} label="FIn"
                onChange={this.handleChange('itemFIn')}
              /></li>
              <li><label>Avg. Wt:</label><input
                style={{margin: 5, width:60}} id="itemAvg"
                placeholder={this.props.Item.avg} label="Avg"
                onChange={this.handleChange('itemAvg')}
              /></li>
              <li><label>F C R:</label><input
                style={{margin: 5, width:60}} id="itemFCR"
                placeholder={this.props.Item.fcr} label="FCR"
                onChange={this.handleChange('itemFCR')}
              /></li>
               <li><label>Bal. Birds:</label><input
                style={{margin: 5, width:60}} id="itemBBirds"
                placeholder={this.props.Item.bbird} label="BBirds"
                onChange={this.handleChange('itemBBirds')}
              /></li>
              <li><label>Complaint:</label><input
                style={{marginRight: 10}} id="itemComplaint"
                placeholder={this.props.Item.complaint} label="Complaint"
                onChange={this.handleChange('itemComplaint')}
              /></li>
              <li><label>Medicine:</label><input
                style={{marginTop: 10}} id="itemMedicine"
                placeholder={this.props.Item.medicine} label="Medicine"
                onChange={this.handleChange('itemMedicine')}
              /></li></ul>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.handleSubmit} variant="success">Update</Button>
            <Button onClick={this.handleClose} variant="danger">Cancel</Button>
          </Modal.Footer>
        </Modal>
      </form>
    );
  }
}export default Editor;
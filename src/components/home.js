import React, { useState, useEffect} from 'react';
import {listDailyentrys} from '../graphql/queries'
import { API, graphqlOperation} from 'aws-amplify';
import {Card, CardGroup} from 'react-bootstrap'

function Home() {

  const [notes, setNotes] = useState([]);
  const [search, setSearch] = useState("");
  const [filtered, setFiltered] = useState([]);


  useEffect(() => {
    fetchNotes()
  }, []);

  useEffect(() => {
    searchfilter()
  });

    async function fetchNotes() {
      // const apiData = await API.graphql({ query: listDailyentrys });
      const apiData =await API.graphql(graphqlOperation(listDailyentrys, {  limit: 100, sortDirection: 'ASC' }));
      setNotes(apiData.data.listDailyentrys.items);
    }

    const excludeColumns = ["id"];
    async function searchfilter(){
      if (search === "") setFiltered(notes);
      else {
        const filteredData = notes.filter(item => {
          return Object.keys(item).some(key =>
            excludeColumns.includes(key) ? false : item[key].toString().toLowerCase().includes(search.toLowerCase())
          );
        });
        setFiltered(filteredData);
      }
    }


 return (<>
        <div className="container">
        <form >
            <div className="search row mt-3">
                <div className="col-sm-4 p-1 input-group">
                    <input className="searchTerm form-control"  onChange={(e) => setSearch(e.target.value)} 
                       placeholder="What are U Looking for"/>
                    <span className="input-group-append searchButton"><i className="fa fa-search"></i></span>
                </div>
            </div>
        </form>
      </div>
    <div style={{marginBottom: 30}}>
        <div className="col m-2" >
            <CardGroup>
                {filtered.map((note, idx) => (
                <DesignList  key={idx} {...note} />
                ))}
            </CardGroup>
        </div>
    </div>
 </> );
}
const DesignList = (card) => {
  return(<>
        <Card border="danger" style={{ width: '18rem' }} className="col-sm-3" >
          <Card.Header className="text-uppercase">Farm: <b>{card.fname}</b> User: <b>{card.uname}</b></Card.Header>
          <Card.Body>
              <Card.Text>
              <ul>
                <li  className="text-primary">Date: <b>{card.createdAt}</b></li>
              </ul>
              <ul>
              <li>NOC: <b>{card.tchicks} </b> </li>
              <li  className="text-success">Age: <b>{card.age}</b></li>
              <li  className="text-danger">Cum.Mor: <b>{card.cmor}</b></li>
              <li  className="text-primary">Balance Feed: <b>{card.fstock}</b></li>
              <li  className="text-success">Average Wt.: <b>{card.avg}</b></li>
              <li  className="text-success">Balance Birds: <b>{card.bbird}</b></li>
              </ul>
          </Card.Text>
          </Card.Body>
      </Card>
  </>);
}
export default Home ;
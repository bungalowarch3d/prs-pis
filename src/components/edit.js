import React, { useState, useEffect} from 'react';
import {listDailyentrys} from '../graphql/queries'
import {deleteDailyentry} from '../graphql/mutations'
import { API} from 'aws-amplify';
import {Card, CardGroup, Button} from 'react-bootstrap'
import Editor from '../components/editor'

function Edit() {

  const [notes, setNotes] = useState([]);
  const [search, setSearch] = useState("");
  const [filtered, setFiltered] = useState([]);

  useEffect(() => {
      fetchNotes()
    }, []);

    useEffect(() => {
      searchfilter()
    });
    async function fetchNotes() {

      const apiData = await API.graphql({ query: listDailyentrys });
      setNotes(apiData.data.listDailyentrys.items);
    }

    async function deleteNote({ id }) {
      const newNotesArray = notes.filter(card => card.id !== id);
      setNotes(newNotesArray);
      await API.graphql({ query: deleteDailyentry, variables: { input: { id } }});
    }

    const excludeColumns = ["id"];
    async function searchfilter(){
      if (search === "") setFiltered(notes);
      else {
        const filteredData = notes.filter(item => {
          return Object.keys(item).some(key =>
            excludeColumns.includes(key) ? false : item[key].toString().toLowerCase().includes(search.toLowerCase())
          );
        });
        setFiltered(filteredData);
      }
    }

 return (<>
      <div className="container">
      <form>
          <div className="search row mt-3">
              <div className="col-sm-4 p-1 input-group">
                  <input className="searchTerm form-control" onChange={(e) => setSearch(e.target.value)} placeholder="Search Any: Farm / User / Date / Age...."/>
                  <span className="input-group-append searchButton"><i className="fa fa-search"></i></span>
              </div>
          </div>
      </form>
    </div>
    <div style={{marginBottom: 30}}>
        <div className="col m-2" >
            <CardGroup>
                {filtered.map((card, idx) => (
                // <DesignList  key={idx} {...note} />
                <Card border="danger" style={{ width: '18rem' }} className=" col-sm-3" >
                <Card.Header className="text-uppercase">Farm: <b>{card.fname}</b>  User: <i>{card.uname}</i></Card.Header>
                <Card.Body>
                    <Card.Text>
                      <ul>
                        <li  className="text-primary">Date: <b>{card.createdAt}</b></li>
                        <li  className="text-secondary">Edited: <b>{card.updatedAt}</b></li>
                      </ul>
                      <ul>
                        <li>Chicks:  <b>{card.chicks} </b></li>
                        <li>NOC:  <b>{card.tchicks} </b> </li>
                        <li  className="text-success">Age: <b>{card.age}</b></li>
                        <li  className="text-info">Mortality: <b>{card.mortality}</b></li>
                        <li  className="text-danger">Cum.Mor: <b>{card.cmor}</b></li>
                        <li  className="">Feed Used: <b>{card.fused}</b></li>
                        <li  className="text-primary">Balance Feed: <b>{card.fstock}</b></li>
                        <li  className="">Total Feed: <b>{card.cfeed}</b></li>
                        <li  className="">Feed In-Take: <b>{card.fintake}</b></li>
                        <li  className="text-success">Average Wt.: <b>{card.avg}</b></li>
                        <li  className="text-info">FCR: <b>{card.fcr}</b></li>
                        <li  className="text-success">Balance Birds: <b>{card.bbird}</b></li>
                        <li  className="text-danger">Complaints: <b>{card.complaint}</b></li>
                        <li  className="text-secondary">Medicines: <b>{card.medicine}</b></li>
                      </ul>
                  </Card.Text>
                  <Card.Footer>
                    <div className="row float-right">
                    <Editor  Item={card}/>
                    <Button className="btn mb-1" variant="danger" onClick={() => {if(window.confirm('Delete the item?')){deleteNote(card)};} }>Delete</Button>
                  </div></Card.Footer>
                </Card.Body>
            </Card>
                ))}
            </CardGroup>
        </div>
    </div>
 </> );
}
export default Edit ;
import React, { useState, useEffect} from 'react';
import {API,Auth, graphqlOperation } from 'aws-amplify';
import { createDailyentry as createNoteMutation} from '../graphql/mutations';
import {listDailyentrys} from '../graphql/queries'
import {Card, CardGroup} from 'react-bootstrap'


const initialFormState = { uname: '', fname: ''
, chicks:'', tchicks:'', age : '', mortality : '',
                            cmor : '',fused : '', fstock : '', cfeed : '',fintake : '', avg : '', fcr : '', 
                            bbird : '', complaint : '',  medicine : '' }

function AddNew (){
    const [notes, setNotes] = useState([]);
    const [formData, setFormData] = useState(initialFormState);
    const [todos, setTodos] = useState([])

    useEffect(() => {
        fetchNotes()
      }, []);

      async function fetchNotes() {
        const apiData = await API.graphql({ query: listDailyentrys });
        setNotes(apiData.data.listDailyentrys.items);
      }


      async function createNote() {
        try {
            const todo = { ...formData }
            setTodos([...todos, todo])
            todo.uname = Auth.user.username
            setFormData(initialFormState);
            await API.graphql(graphqlOperation(createNoteMutation, {input: todo}))
            //console.log(todo)
            console.log("Success")
            fetchNotes()
        } catch (err) {
            console.log('error creating todo:', err)
      }
      }
 
    return(<>
        <div className="container">
            
            <div className="form-group row">
                <label className="col-sm-1 col-form-label"></label>
                <div className="col-sm m-4">
                    <h3>Farm Daily Entry </h3>
                </div>
            </div>
            <div className="form-group row">
                <input className="col-sm-3 m-2" placeholder="Farmer Name" 
                     onChange={e => setFormData({ ...formData, 'fname': e.target.value})}
                     value={formData.fname}/>
                <input className="col-sm-3 m-2" placeholder="Chicks Source" 
                onChange={e => setFormData({ ...formData, 'chicks': e.target.value})}
                value={formData.chicks}/>
                <input className="col-sm-3 m-2" placeholder="Total Chicks" 
                onChange={e => setFormData({ ...formData, 'tchicks': e.target.value})}
                value={formData.tchicks}/>
                <input className="col-sm-3 m-2" placeholder="Birds Age" 
                onChange={e => setFormData({ ...formData, 'age': e.target.value})}
                value={formData.age}/>
                <input className="col-sm-3 m-2" placeholder="Mortality" 
                onChange={e => setFormData({ ...formData, 'mortality': e.target.value})}
                value={formData.mortality}/>
                <input className="col-sm-3 m-2" placeholder="Cum. Mortality" 
                onChange={e => setFormData({ ...formData, 'cmor': e.target.value})}
                value={formData.cmor}/>
                <input className="col-sm-3 m-2" placeholder="Feed Used" 
                onChange={e => setFormData({ ...formData, 'fused': e.target.value})}
                value={formData.fused}/>
                <input className="col-sm-3 m-2" placeholder="Feed Stock"
                onChange={e => setFormData({ ...formData, 'fstock': e.target.value})}
                value={formData.fstock}/>
                <input className="col-sm-3 m-2" placeholder="Total Feed"
                onChange={e => setFormData({ ...formData, 'cfeed': e.target.value})}
                value={formData.cfeed}/>
                <input className="col-sm-3 m-2" placeholder="Feed In-Take"
                onChange={e => setFormData({ ...formData, 'fintake': e.target.value})}
                value={formData.fintake}/>
                <input className="col-sm-3 m-2" placeholder="Bird Avg. Wt"
                onChange={e => setFormData({ ...formData, 'avg': e.target.value})}
                value={formData.avg}/>
                <input className="col-sm-3 m-2" placeholder="Birds FCR"
                onChange={e => setFormData({ ...formData, 'fcr': e.target.value})}
                value={formData.fcr}/>
                <input className="col-sm-3 m-2" placeholder="Balance Birds"
                onChange={e => setFormData({ ...formData, 'bbird': e.target.value})}
                value={formData.bbird}/>
                <input className="col-sm-3 m-2" placeholder="Complaint"
                onChange={e => setFormData({ ...formData, 'complaint': e.target.value})}
                value={formData.complaint}/>
                <input className="col-sm-3 m-2" placeholder="Medicine Used"
                onChange={e => setFormData({ ...formData, 'medicine': e.target.value})}
                value={formData.medicine}/>
            </div>
            <div className=" row float-center">
                <button onClick={createNote} className="col-sm-3 btn btn-outline-success form-contro">Save</button> 
            </div>  
        </div>  
    <div style={{marginBottom: 30}}>
        <div className="col m-2" >
            <CardGroup>
                {notes.map((note, idx) => (
                <DesignList  key={idx} {...note} />
                ))}
            </CardGroup>
        </div>
    </div>

</>); }
const DesignList = (card) => {
    return(<>

          <Card border="danger" style={{ width: '18rem' }} className="col-sm-3" >
            <Card.Header className="text-uppercase">Farmer: <b>{card.fname}</b></Card.Header>
            <Card.Body>
                <Card.Text>
                Entry By: <span className="text-uppercase text-primary"><b>{card.owner}</b></span> On: <i className="text-secondary"><b>{card.createdAt}</b></i>
            </Card.Text>
            </Card.Body>
        </Card>
    </>);
}
export default AddNew;
import React, { Component } from 'react';
import {Navbar, Nav} from 'react-bootstrap'
import { Auth} from 'aws-amplify';

class Navigator  extends Component {

    async handleLogout() {
        try {
            await Auth.signOut({ global: true });
            window.location.reload()
          } catch (error) {
            console.log('error signing out: ', error);
          }
    };

    render(){
        return(<>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" className="navbar">
                <Navbar.Brand >Poultry Inventory <i>~ PRS</i></Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link exact='True' className="mr-4 " href="/" > Home</Nav.Link>
                        <Nav.Link exact='True' className="mr-4 " href="/add">Add New</Nav.Link>
                        <Nav.Link exact='True' className="mr-4 " href="/edit">Options</Nav.Link>
                        <Navbar.Text className="mr-4 "> Signed in as: {Auth.user.username}</Navbar.Text> 
                        <Nav.Link className="mr-4 " onClick={this.handleLogout}>Logout</Nav.Link>                        
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
       </> );
    }
}
export default Navigator ;
import React from 'react';
import Amplify from "@aws-amplify/core";
import { withAuthenticator } from '@aws-amplify/ui-react';
import {BrowserRouter,Route,Switch} from 'react-router-dom';

import './App.css';
import aws_exports from "./aws-exports"
import Navigator from './components/NavBar'
import Home from './components/home'
import AddNew  from "./components/AddNew";
import Edit from './components/edit'
import Editor from './components/editor'

Amplify.configure(aws_exports);



function App() {
  // console.log(Auth.user.attributes.email)


  return (
    <>
    {/* <AmplifySignOut /> */}
    <BrowserRouter>
      <Navigator />
        <Switch>
           {/* <Route path="/" component={Home}/> */}
          <Route exact path="/"> <Home /> </Route>
          <Route exact path="/add"> <AddNew /> </Route>
          <Route exact path="/edit"> <Edit /> </Route>
          <Route exact path="/editor"> <Editor /></Route>
        </Switch>
      </BrowserRouter>
    </>
  );
}

export default withAuthenticator(App);